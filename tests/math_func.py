#!/usr/bin/env python3

def add(x, y = 2) :
    return x + y

def product(x, y = 2) :
    return x * y
