#!/usr/bin/env python3

ma_liste = [1, 2, 3, 4, 5]
ma_liste2 = [8, 9, 10]

ma_liste.append(56)
ma_liste.insert(4, 100)

print(ma_liste[3])
print(ma_liste)

ma_liste += ma_liste2
print(ma_liste)
del ma_liste[3]
print(ma_liste)

for elt in enumerate(ma_liste):
    print(elt)

liste_origine = [0, 1, 2, 3, 4, 5]
liste_origine = [nb * nb for nb in liste_origine]
print('\n', *liste_origine, '\n')

liste_origine = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
print(liste_origine)
liste_origine = [nb for nb in liste_origine if nb % 2 == 0]
print(liste_origine, '\n')

qtt_a_retirer = 7
fruits_stockes = [15, 3, 18, 21]
print(*fruits_stockes)
fruits_stockes = [nb_fruits - qtt_a_retirer for nb_fruits in fruits_stockes if nb_fruits>qtt_a_retirer]
print(*fruits_stockes)


inventaire = [
    ("fraises", 76),
    ("prunes", 51),
    ("pommes", 22),
    ("poires", 18),
    ("melons", 4),
]
print(inventaire)

