#!/usr/bin/env python3

def dom():
    
    annee = input("Saisissez une année : ")
    annee = int(annee)
    bissextile = False

    if annee % 400 == 0:
        bissextile = True
    elif annee % 100 == 0:
        bissextile = False
    elif annee % 4 == 0:
        bissextile = True
    else:
        bissextile = False
    if bissextile:
        print("L'année saisie est bissextile.")
    else:
        print("L'année saisie n'est pas bissextile.")

def opti():
    
    annee = input("Saisissez une année : ")
    annee = int(annee)

    if annee % 400 == 0 or (annee % 4 == 0 and annee % 100 != 0):
        print("L'année saisie est bissextile.")
    else:
        print("L'année saisie n'est pas bissextile.")

dom()
opti()
