#!/usr/bin/env python3

mon_dictionnaire = {}
mon_dictionnaire["pseudo"] = "Prolixe"
mon_dictionnaire["mot de passe"] = "*"
mon_dictionnaire["pseudo"] = "6pri1"
print(mon_dictionnaire)
print("mot de pass = ", mon_dictionnaire["mot de passe"])

mon_dictionnaire = {}
mon_dictionnaire[0] = "a"
mon_dictionnaire[1] = "e"
mon_dictionnaire[2] = "i"
mon_dictionnaire[3] = "o"
mon_dictionnaire[4] = "u"
mon_dictionnaire[5] = "y"
print(mon_dictionnaire)

placard = {"chemise":3, "pantalon":6, "tee-shirt":7}
print("placard = ", placard)
del placard["chemise"]
print("new placard = ", placard)


def fete():
    print("C'est la fête.\n")

def oiseau():
    print("Fais comme l'oiseau...\n")

fonctions = {}
fonctions["fete"] = fete
fonctions["oiseau"] = oiseau
fonctions["oiseau"]
fonctions["oiseau"]()

fruits = {"pommes":21, "melons":3, "poires":31}
for cle in fruits:
    print(cle)
for valeur in fruits.values():
    print(valeur)
for cle, valeur in fruits.items():
    print("La clé {} contient la valeur {}.\n".format(cle, valeur))

def fonction_inconnue(**parametres_nommes):
    print("J'ai reçu en paramètres nommés : {}.".format(parametres_nommes))
fonction_inconnue(p=4, j=8)

parametres = {"sep":" >> ", "end":" -\n"}
print("Voici", "un", "exemple", "d'appel", **parametres)
