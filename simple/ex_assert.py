#!/usr/bin/env python3

annee = input("Saisissez une année supérieure à 0 :")
try:
    annee = int(annee)
    assert annee > 0
except ValueError:
    print("Vous n'avez pas saisi un nombre.")
except AssertionError:
    print("L'année saisie est inférieure ou égale à 0.")
