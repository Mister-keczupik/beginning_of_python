#!/usr/bin/env python3

numerateur = int(input("le numerateur est "))
denominateru = int(input("le denominateur est "))
resulta = int(input("le resulta est "))

try:
    resultat = numerateur / denominateur
except NameError:
    print("La variable numerateur ou denominateur n'a pas été définie.")
except:
    pass
except TypeError:
    print("La variable numerateur ou denominateur possède un type incompatible avec la division.")
except ZeroDivisionError:
    print("La variable denominateur est égale à 0.")
else:
    print("Le résultat obtenu est", resultat)
