#!/usr/bin/env python3

import pickle

score = {
  "joueur 1":    5,
  "joueur 2":   35,
  "joueur 3":   20,
  "joueur 4":    2,
}

with open('donnees', 'wb') as fichier:
    mon_pickler = pickle.Pickler(fichier)
    mon_pickler.dump(score)
